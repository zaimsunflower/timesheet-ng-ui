import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { SubSink } from 'subsink';

import { ProjectState } from '../_store/state.index';
import { selectTimesheetById } from '../_store/timesheet/timesheet.selector';
import { updateTimesheetInit } from '../_store/timesheet/timesheet.action';
import { selectUser } from '../_store/user/user.selector';
import { selectTask } from '../_store/task/task.selector';
import { selectStatus } from '../_store/status/status.selector';

import { Timesheet } from '../_models/timesheet.model';
import { Task } from '../_models/task.model';
import { Status } from '../_models/status.model';
import { User } from '../_models/user.model';

export interface DialogData {
  id: string;
}

@Component({
  selector: 'app-edit-modal',
  templateUrl: './edit-modal.component.html',
  styleUrls: ['./edit-modal.component.css'],
})
export class EditModalComponent implements OnInit, OnDestroy {
  private subs: SubSink = new SubSink();
  statuses$: Observable<Status[]>;
  tasks$: Observable<Task[]>;
  users$: Observable<User[]>;
  currentTimesheet: Timesheet;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private store: Store<ProjectState>
  ) {}
  editTimesheetForm = new FormGroup({
    projectName: new FormControl('', {
      validators: [Validators.required],
    }),
    taskName: new FormControl('', {
      validators: [Validators.required],
    }),
    dateFrom: new FormControl(new Date(), {
      validators: [Validators.required],
    }),
    dateTo: new FormControl(new Date(), {
      validators: [Validators.required],
    }),
    status: new FormControl('', {
      validators: [Validators.required],
    }),
    assignTo: new FormControl('', {
      validators: [Validators.required],
    }),
  });

  ngOnInit() {
    this.statuses$ = this.store.select(selectStatus);
    this.tasks$ = this.store.select(selectTask);
    this.users$ = this.store.select(selectUser);

    this.subs.sink = this.store
      .select(selectTimesheetById(this.data.id))
      .subscribe((timesheet) => {
        this.currentTimesheet = timesheet;
        this.editTimesheetForm.patchValue({
          projectName: timesheet.project,
          taskName: timesheet.taskId,
          dateFrom: timesheet.fromDate,
          dateTo: timesheet.toDate,
          status: timesheet.statusId,
          assignTo: timesheet.userId,
        });
      });
  }

  onSave() {
    const updatedTimesheet: Timesheet = {
      project: this.editTimesheetForm.value.projectName,
      taskId: this.editTimesheetForm.value.taskName,
      fromDate: this.editTimesheetForm.value.dateFrom,
      toDate: this.editTimesheetForm.value.dateTo,
      statusId: this.editTimesheetForm.value.status,
      userId: this.editTimesheetForm.value.assignTo,
    };
    this.store.dispatch(
      updateTimesheetInit({
        timesheetId: this.currentTimesheet.id,
        timesheet: updatedTimesheet,
      })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
