export interface Timesheet {
  id?: string;
  project: string;
  userId: string;
  taskId?: string;
  statusId: string;
  fromDate: Date;
  toDate: Date;
}
