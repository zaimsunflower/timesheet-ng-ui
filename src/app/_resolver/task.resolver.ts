import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, of, tap } from 'rxjs';

import { ProjectState } from '../_store/state.index';
import { selectTask } from '../_store/task/task.selector';
import { getTaskInit } from '../_store/task/task.action';
import { Task } from '../_models/task.model';

@Injectable({
  providedIn: 'root',
})
export class TaskResolver {
  constructor(private store: Store<ProjectState>) {}
  resolve(route: ActivatedRouteSnapshot): Observable<Task[]> {
    return this.store.select(selectTask).pipe(
      tap((questions) => {
        if (questions.length == 0) {
          this.store.dispatch(getTaskInit());
        }
      })
    );
  }
}
