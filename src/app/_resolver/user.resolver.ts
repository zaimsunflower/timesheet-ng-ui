import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, tap } from 'rxjs';

import { ProjectState } from '../_store/state.index';
import { selectUser } from '../_store/user/user.selector';
import { getUserInit } from '../_store/user/user.action';
import { User } from '../_models/user.model';

@Injectable({
  providedIn: 'root',
})
export class UserResolver {
  constructor(private store: Store<ProjectState>) {}
  resolve(route: ActivatedRouteSnapshot): Observable<User[]> {
    return this.store.select(selectUser).pipe(
      tap((questions) => {
        if (questions.length == 0) {
          this.store.dispatch(getUserInit());
        }
      })
    );
  }
}
