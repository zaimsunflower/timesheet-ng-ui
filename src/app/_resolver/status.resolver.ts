import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, tap } from 'rxjs';

import { Status } from '../_models/status.model';
import { ProjectState } from '../_store/state.index';
import { selectStatus } from '../_store/status/status.selector';
import { getStatusInit } from '../_store/status/status.action';

@Injectable({
  providedIn: 'root',
})
export class StatusResolver {
  constructor(private store: Store<ProjectState>) {}
  resolve(route: ActivatedRouteSnapshot): Observable<Status[]> {
    return this.store.select(selectStatus).pipe(
      tap((questions) => {
        if (questions.length == 0) {
          this.store.dispatch(getStatusInit());
        }
      })
    );
  }
}
