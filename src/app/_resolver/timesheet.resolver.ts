import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, tap } from 'rxjs';

import { ProjectState } from '../_store/state.index';
import { selectTimesheet } from '../_store/timesheet/timesheet.selector';
import { getTimesheetInit } from '../_store/timesheet/timesheet.action';
import { Timesheet } from '../_models/timesheet.model';

@Injectable({
  providedIn: 'root',
})
export class TimesheetResolver {
  constructor(private store: Store<ProjectState>) {}
  resolve(route: ActivatedRouteSnapshot): Observable<Timesheet[]> {
    return this.store.select(selectTimesheet).pipe(
      tap((timesheet) => {
        if (timesheet.length == 0) {
          this.store.dispatch(getTimesheetInit());
        }
      })
    );
  }
}
