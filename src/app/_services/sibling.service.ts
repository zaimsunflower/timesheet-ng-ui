import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SiblingService {
  private applyFilter = new BehaviorSubject<any>(null);

  keyStroke$ = this.applyFilter.asObservable();

  nextApplyFilter(event: Event) {
    this.applyFilter.next(event);
  }
}