import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../environment/environment';
import { Task } from '../_models/task.model';

@Injectable({
  providedIn: 'root',
})
export class TaskService {
  constructor(private http: HttpClient) {}

  erroHandler(error: HttpErrorResponse) {
    return throwError(() => new Error(error.message || 'server Error'));
  }

  getTask(): Observable<Task[]> {
    const headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
    });
    return this.http
      .get<Task[]>(`${environment.TIMESHEET_API}/tasks`, {
        headers: headers,
      })
      .pipe(catchError(this.erroHandler));
  }
  createTask(task: Task): Observable<Task> {
    const headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
    });
    return this.http.post<Task>(`${environment.TIMESHEET_API}/tasks`, {
      ...task,
    });
  }
}
