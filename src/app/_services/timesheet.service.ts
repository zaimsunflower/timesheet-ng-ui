import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../environment/environment';
import { Timesheet } from '../_models/timesheet.model';

@Injectable({
  providedIn: 'root',
})
export class TimesheetService {
  constructor(private http: HttpClient) {}

  erroHandler(error: HttpErrorResponse) {
    return throwError(() => new Error(error.message || 'server Error'));
  }

  getTimesheet(): Observable<Timesheet[]> {
    const headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
    });
    return this.http
      .get<Timesheet[]>(`${environment.TIMESHEET_API}/timesheet`, {
        headers: headers,
      })
      .pipe(catchError(this.erroHandler));
  }

  deleteTimesheet(timesheetId: string): Observable<string> {
    const headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
    });
    return this.http.delete<string>(
      `${environment.TIMESHEET_API}/timesheet/${timesheetId}`
    );
  }

  createTimesheet(timesheet: Timesheet): Observable<Timesheet> {
    const headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
    });
    return this.http.post<Timesheet>(`${environment.TIMESHEET_API}/timesheet`, {
      ...timesheet,
    });
  }

  updateTimesheet(
    timesheetId: string,
    timesheet: Timesheet
  ): Observable<Timesheet> {
    const headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
    });
    return this.http.put<Timesheet>(
      `${environment.TIMESHEET_API}/timesheet/${timesheetId}`,
      {
        ...timesheet,
      }
    );
  }
}
