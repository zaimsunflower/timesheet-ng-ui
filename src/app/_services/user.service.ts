import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../environment/environment';
import { User } from '../_models/user.model';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  erroHandler(error: HttpErrorResponse) {
    return throwError(() => new Error(error.message || 'server Error'));
  }

  getUser(): Observable<User[]> {
    const headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
    });
    return this.http
      .get<User[]>(`${environment.TIMESHEET_API}/users`, {
        headers: headers,
      })
      .pipe(catchError(this.erroHandler));
  }
}
