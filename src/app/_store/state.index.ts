import { StatusState } from './status/status.state';
import { TaskState } from './task/task.state';
import { TimesheetState } from './timesheet/timesheet.state';
import { UserState } from './user/user.state';

export interface ProjectState {
  status: StatusState;
  timesheet: TimesheetState;
  task: TaskState;
  user: UserState;
}
