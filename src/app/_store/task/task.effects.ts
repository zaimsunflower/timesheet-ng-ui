import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { getTaskFailure, getTaskInit, getTaskSuccess } from './task.action';
import { Task } from '../../_models/task.model';
import { TaskService } from '../../_services/task.service';

@Injectable()
export class TaskEffects {
  loadTask$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getTaskInit),
      switchMap((action) => {
        return this.task$.getTask().pipe(
          map((res: Task[]) => {
            return getTaskSuccess({ task: res });
          }),
          catchError((err) => of(getTaskFailure({ message: err })))
        );
      })
    )
  );

  constructor(private actions$: Actions, private task$: TaskService) {}
}
