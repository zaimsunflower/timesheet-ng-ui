import { createReducer, on } from '@ngrx/store';

import { initialState } from './task.state';
import { createTaskSuccess, getTaskSuccess } from './task.action';

export const taskReducer = createReducer(
  initialState,
  on(getTaskSuccess, (state, action) => {
    return { ...state, task: action.task };
  }),

  on(createTaskSuccess, (state, action) => {
    return { ...state, task: [...state.task, action.task] };
  })
);
