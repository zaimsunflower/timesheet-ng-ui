import { createAction, props } from '@ngrx/store';

import { Task } from '../../_models/task.model';

const GET_TASK_INIT = '[Task Store] Retrieve All Task Init';
const GET_TASK_SUCCESS = '[Task Store] Retrieve All Task Success';
const GET_TASK_FAILURE = '[Task Store] Retrieve All Task Failure';

const CREATE_TASK_INIT = '[Task Store] Create Task Init';
const CREATE_TASK_SUCCESS = '[Task Store] Create Task Success';
const CREATE_TASK_FAILURE = '[Task Store] Create Task Failure';

export const getTaskInit = createAction(GET_TASK_INIT);
export const getTaskSuccess = createAction(
  GET_TASK_SUCCESS,
  props<{ task: Task[] }>()
);
export const getTaskFailure = createAction(
  GET_TASK_FAILURE,
  props<{ message: string }>()
);

export const createTaskInit = createAction(CREATE_TASK_INIT);
export const createTaskSuccess = createAction(
  CREATE_TASK_SUCCESS,
  props<{ task: Task }>()
);
export const createTaskFailure = createAction(
  CREATE_TASK_FAILURE,
  props<{ message: string }>()
);
