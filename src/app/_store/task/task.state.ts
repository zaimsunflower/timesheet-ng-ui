import { Task } from '../../_models/task.model';

export interface TaskState {
  task: Task[];
}

export const initialState: TaskState = {
  task: [],
};
