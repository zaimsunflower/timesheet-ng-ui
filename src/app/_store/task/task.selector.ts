import { createSelector } from '@ngrx/store';

import { ProjectState } from '../state.index';
import { TaskState } from './task.state';

export const selectTaskState = (state: ProjectState) => state.task;

export const selectTask = createSelector(
  selectTaskState,
  (state: TaskState) => state.task
);
