import { createReducer, on } from '@ngrx/store';

import { initialState } from './status.state';
import { getStatusSuccess } from './status.action';

export const statusReducer = createReducer(
  
  initialState,
  on(getStatusSuccess, (state, action) => {
    return { ...state, status: action.status };
  })
);
