import { Status } from '../../_models/status.model';

export interface StatusState {
  status: Status[];
}

export const initialState: StatusState = {
  status: [],
};
