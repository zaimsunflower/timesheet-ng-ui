import { createSelector } from '@ngrx/store';

import { ProjectState } from '../state.index';
import { StatusState } from './status.state';

export const selectStatusState = (state: ProjectState) => state.status;

export const selectStatus = createSelector(
  selectStatusState,
  (state: StatusState) => state.status
);
