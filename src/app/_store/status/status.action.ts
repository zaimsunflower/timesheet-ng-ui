import { createAction, props } from '@ngrx/store';
import { Status } from '../../_models/status.model';

const GET_STATUS_INIT = '[Status Store] Retrieve All Status Init';
const GET_STATUS_SUCCESS = '[Status Store] Retrieve All Status Success';
const GET_STATUS_FAILURE = '[Status Store] Retrieve All Status Failure';

export const getStatusInit = createAction(GET_STATUS_INIT);
export const getStatusSuccess = createAction(
  GET_STATUS_SUCCESS,
  props<{ status: Status[] }>()
);
export const getStatusFailure = createAction(
  GET_STATUS_FAILURE,
  props<{ message: string }>()
);
