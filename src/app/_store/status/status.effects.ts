import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import {
  getStatusFailure,
  getStatusInit,
  getStatusSuccess,
} from './status.action';
import { Status } from '../../_models/status.model';
import { StatusService } from '../../_services/status.service';

@Injectable()
export class StatusEffects {
  loadStatus$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getStatusInit),
      switchMap((action) => {
        return this.status$.getStatus().pipe(
          map((res: Status[]) => {
            return getStatusSuccess({ status: res });
          }),
          catchError((err) => of(getStatusFailure({ message: err })))
        );
      })
    )
  );

  constructor(private actions$: Actions, private status$: StatusService) {}
}
