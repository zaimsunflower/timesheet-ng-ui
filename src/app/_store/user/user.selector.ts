import { createSelector } from '@ngrx/store';

import { ProjectState } from '../state.index';
import { UserState } from './user.state';

export const selectUserState = (state: ProjectState) => state.user;

export const selectUser = createSelector(
  selectUserState,
  (state: UserState) => state.user
);
