import { createAction, props } from '@ngrx/store';

import { User } from '../../_models/user.model';

const GET_USER_INIT = '[User Store] Retrieve All User Init';
const GET_USER_SUCCESS =
  '[User Store] Retrieve All User Success';
const GET_USER_FAILURE =
  '[User Store] Retrieve All User Failure';

export const getUserInit = createAction(GET_USER_INIT);
export const getUserSuccess = createAction(
  GET_USER_SUCCESS,
  props<{ user: User[] }>()
);
export const getUserFailure = createAction(
  GET_USER_FAILURE,
  props<{ message: string }>()
);
