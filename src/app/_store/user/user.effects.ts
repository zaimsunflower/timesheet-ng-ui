import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { getUserFailure, getUserInit, getUserSuccess } from './user.action';
import { User } from '../../_models/user.model';
import { UserService } from '../../_services/user.service';

@Injectable()
export class UserEffects {
  loadUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getUserInit),
      switchMap((action) => {
        return this.user$.getUser().pipe(
          map((res: User[]) => {
            return getUserSuccess({ user: res });
          }),
          catchError((err) => of(getUserFailure({ message: err })))
        );
      })
    )
  );

  constructor(private actions$: Actions, private user$: UserService) {}
}
