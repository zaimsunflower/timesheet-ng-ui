import { createReducer, on } from '@ngrx/store';

import { initialState } from './user.state';
import { getUserSuccess } from './user.action';

export const userReducer = createReducer(
  initialState,
  on(getUserSuccess, (state, action) => {
    return { ...state, user: action.user };
  })
);
