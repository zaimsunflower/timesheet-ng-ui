import { User } from '../../_models/user.model';

export interface UserState {
  user: User[];
}

export const initialState: UserState = {
  user: [],
};
