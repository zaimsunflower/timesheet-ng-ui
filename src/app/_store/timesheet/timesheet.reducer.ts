import { createReducer, on } from '@ngrx/store';

import { initialState } from './timesheet.state';
import {
  createTimesheetSuccess,
  getTimesheetSuccess,
} from './timesheet.action';

export const timesheetReducer = createReducer(
  initialState,
  on(getTimesheetSuccess, (state, action) => {
    return { ...state, timesheet: action.timesheet };
  }),

  on(createTimesheetSuccess, (state, action) => {
    return {
      ...state,
      timesheet: [...state.timesheet, action.timesheet],
    };
  })
);
