import { createAction, props } from '@ngrx/store';

import { Task } from '../../_models/task.model';
import { Timesheet } from '../../_models/timesheet.model';

const GET_TIMESHEET_INIT = '[Timesheet Store] Retrieve All Timesheet Init';
const GET_TIMESHEET_SUCCESS =
  '[Timesheet Store] Retrieve All Timesheet Success';
const GET_TIMESHEET_FAILURE =
  '[Timesheet Store] Retrieve All Timesheet Failure';

const DELETE_TIMESHEET_INIT = '[Timesheet Store] Delete Timesheet by Id Init';
const DELETE_TIMESHEET_SUCCESS =
  '[Timesheer Store] Delete Timesheet by Id Success';
const DELETE_TIMESHEET_FAILURE =
  '[Timesheet Store] Delete Timesheet by Id Failure';

const CREATE_TIMESHEET_INIT = '[Timesheet Store] Create Timesheet Init';
const CREATE_TIMESHEET_SUCCESS = '[Timesheet Store] Create Timesheet Success';
const CREATE_TIMESHEET_FAILURE = '[Timesheet Store] Create Timesheet Failure';

const UPDATE_TIMESHEET_INIT = '[Timesheet Store] Update Timesheet Init';
const UPDATE_TIMESHEET_SUCCESS = '[Timesheet Store] Update Timesheet Success';
const UPDATE_TIMESHEET_FAILURE = '[Timesheet Store] Update Timesheet Failure';

export const getTimesheetInit = createAction(GET_TIMESHEET_INIT);
export const getTimesheetSuccess = createAction(
  GET_TIMESHEET_SUCCESS,
  props<{ timesheet: Timesheet[] }>()
);
export const getTimesheetFailure = createAction(
  GET_TIMESHEET_FAILURE,
  props<{ message: string }>()
);

export const deleteTimesheetInit = createAction(
  DELETE_TIMESHEET_INIT,
  props<{ timesheetId: string }>()
);
export const deleteTimesheetSuccess = createAction(
  DELETE_TIMESHEET_SUCCESS,
  props<{ timesheetId: string }>()
);
export const deleteTimsheetFailure = createAction(
  DELETE_TIMESHEET_FAILURE,
  props<{ message: string }>()
);

export const createTimesheetInit = createAction(
  CREATE_TIMESHEET_INIT,
  props<{ task: Task; timesheet: Timesheet }>()
);

export const createTimesheetSuccess = createAction(
  CREATE_TIMESHEET_SUCCESS,
  props<{ timesheet: Timesheet }>()
);

export const createTimesheetFailure = createAction(
  CREATE_TIMESHEET_FAILURE,
  props<{ message: string }>()
);

export const updateTimesheetInit = createAction(
  UPDATE_TIMESHEET_INIT,
  props<{ timesheetId: string; timesheet: Timesheet }>()
);

export const updateTimesheetSuccess = createAction(
  UPDATE_TIMESHEET_SUCCESS,
  props<{ timesheet: Timesheet }>()
);

export const updateTimesheetFailure = createAction(
  UPDATE_TIMESHEET_FAILURE,
  props<{ message: string }>()
);
