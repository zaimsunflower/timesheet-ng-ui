import { Timesheet } from '../../_models/timesheet.model';

export interface TimesheetState {
  timesheet: Timesheet[];
}

export const initialState: TimesheetState = {
  timesheet: []
};
