import { createSelector } from '@ngrx/store';

import { ProjectState } from '../state.index';
import { TimesheetState } from './timesheet.state';

export const selectTimesheetState = (state: ProjectState) => state.timesheet;

export const selectTimesheet = createSelector(
  selectTimesheetState,
  (state: TimesheetState) => state.timesheet
);

export const selectTimesheetById = (timesheetId: string) =>
  createSelector(selectTimesheetState, (state: TimesheetState) =>
    state.timesheet.find((ts) => ts.id == timesheetId)
  );
