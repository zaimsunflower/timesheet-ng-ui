import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, map, mergeMap, switchMap } from 'rxjs/operators';

import {
  createTimesheetFailure,
  createTimesheetInit,
  createTimesheetSuccess,
  deleteTimesheetInit,
  deleteTimesheetSuccess,
  deleteTimsheetFailure,
  getTimesheetFailure,
  getTimesheetInit,
  getTimesheetSuccess,
  updateTimesheetFailure,
  updateTimesheetInit,
  updateTimesheetSuccess,
} from './timesheet.action';
import { createTaskFailure, createTaskSuccess } from '../task/task.action';
import { TimesheetService } from '../../_services/timesheet.service';
import { TaskService } from '../../_services/task.service';
import { Timesheet } from '../../_models/timesheet.model';
import { Task } from '../../_models/task.model';
import { ProjectState } from '../state.index';

@Injectable()
export class TimesheEtffects {
  loadTimesheet$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getTimesheetInit, updateTimesheetSuccess, deleteTimesheetSuccess),
      switchMap((action) => {
        return this.timesheet$.getTimesheet().pipe(
          map((res: Timesheet[]) => {
            return getTimesheetSuccess({ timesheet: res });
          }),
          catchError((err) => of(getTimesheetFailure({ message: err })))
        );
      })
    )
  );

  deleteTimesheet$ = createEffect(() =>
    this.actions$.pipe(
      ofType(deleteTimesheetInit),
      switchMap((action) => {
        return this.timesheet$.deleteTimesheet(action.timesheetId).pipe(
          map((timesheetId) => {
            return deleteTimesheetSuccess({ timesheetId: timesheetId });
          }),
          catchError((err) => of(deleteTimsheetFailure({ message: err })))
        );
      })
    )
  );

  createTimesheet$ = createEffect(() =>
    this.actions$.pipe(
      ofType(createTimesheetInit),
      switchMap((action) => {
        return this.task$.createTask(action.task).pipe(
          mergeMap((taskRes: Task) => {
            const timesheetPayload: Timesheet = {
              ...action.timesheet,
              taskId: taskRes.id,
            };
            this.store.dispatch(createTaskSuccess({ task: taskRes }));
            return this.timesheet$.createTimesheet(timesheetPayload).pipe(
              map((timesheetRes) => {
                return createTimesheetSuccess({ timesheet: timesheetRes });
              }),
              catchError((err) => of(createTimesheetFailure({ message: err })))
            );
          }),
          catchError((err) => of(createTaskFailure({ message: err })))
        );
      })
    )
  );

  updateTimesheet$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateTimesheetInit),
      switchMap((action) => {
        return this.timesheet$
          .updateTimesheet(action.timesheetId, action.timesheet)
          .pipe(
            map(
              (res: Timesheet) => updateTimesheetSuccess({ timesheet: res }),
              catchError((err) => of(updateTimesheetFailure))
            )
          );
      })
    )
  );

  constructor(
    private actions$: Actions,
    private timesheet$: TimesheetService,
    private task$: TaskService,
    private store: Store<ProjectState>
  ) {}
}
