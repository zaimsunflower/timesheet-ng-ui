import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { combineLatest, filter } from 'rxjs';
import { SubSink } from 'subsink';

import { ProjectState } from '../_store/state.index';
import { selectStatus } from '../_store/status/status.selector';
import { selectUser } from '../_store/user/user.selector';
import { selectTask } from '../_store/task/task.selector';
import { selectTimesheet } from '../_store/timesheet/timesheet.selector';
import { deleteTimesheetInit } from '../_store/timesheet/timesheet.action';

import { Timesheet } from '../_models/timesheet.model';
import { SiblingService } from '../_services/sibling.service';
import { EditModalComponent } from '../edit-modal/edit-modal.component';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.css'],
})
export class ListingComponent implements OnInit, OnDestroy {
  private subs: SubSink = new SubSink();
  dataSource: MatTableDataSource<Timesheet>;
  displayedColumns = [
    'project',
    'user',
    'task',
    'status',
    'fromDate',
    'toDate',
    'action',
  ];
  constructor(
    private siblingService: SiblingService,
    private store: Store<ProjectState>,
    public dialog: MatDialog
  ) {
    this.dataSource = new MatTableDataSource<Timesheet>();
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  ngOnInit(): void {
    this.subs.sink = combineLatest([
      this.store.select(selectStatus),
      this.store.select(selectUser),
      this.store.select(selectTask),
      this.store.select(selectTimesheet),
    ])
      .pipe(
        filter(
          ([a, b, c, d]) =>
            a.length > 0 && b.length > 0 && c.length > 0 && d.length > 0
        )
      )
      .subscribe(([status, user, task, timesheet]) => {
        if (status !== undefined && status.length > 0) {
          const timesheetMapped = timesheet.map((t) => {
            const userInstance = user.find((u) => u.id === t.userId);
            return {
              ...t,
              status: status.find((s) => s.id === t.statusId)?.name ?? '',
              user: userInstance.firstName + ' ' + userInstance.lastName,
              task: task.find((ta) => ta.id === t.taskId)?.name ?? '',
            };
          });
          this.dataSource.data = timesheetMapped;
        }
      });

    this.siblingService.keyStroke$
      .pipe(filter((k) => !!k))
      .subscribe((event) => {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
          this.dataSource.paginator.firstPage();
        }
      });
    this.dataSource.sortingDataAccessor = (
      data: any,
      sortHeaderId: string
    ): string => {
      if (typeof data[sortHeaderId] === 'string') {
        return data[sortHeaderId].toLocaleLowerCase();
      }
      return data[sortHeaderId];
    };
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  editTimesheet(
    id: string,
    enterAnimationDuration: string,
    exitAnimationDuration: string
  ) {
    this.dialog.open(EditModalComponent, {
      enterAnimationDuration,
      exitAnimationDuration,
      data: {
        id: id,
      },
    });
  }

  deleteTimesheet(id: string) {
    this.store.dispatch(deleteTimesheetInit({ timesheetId: id }));
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
