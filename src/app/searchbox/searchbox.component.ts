import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { SiblingService } from '../_services/sibling.service';
import { CreateModalComponent } from '../create-modal/create-modal.component';

@Component({
  selector: 'app-searchbox',
  templateUrl: './searchbox.component.html',
  styleUrls: ['./searchbox.component.css'],
})
export class SearchboxComponent implements OnInit, OnDestroy {
  constructor(
    private siblingService: SiblingService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {}

  applyFilter(event: Event) {
    this.siblingService.nextApplyFilter(event);
  }

  openCreateModalDialog(
    enterAnimationDuration: string,
    exitAnimationDuration: string
  ): void {
    this.dialog.open(CreateModalComponent, {
      enterAnimationDuration,
      exitAnimationDuration,
    });
  }

  ngOnDestroy(): void {}
}
