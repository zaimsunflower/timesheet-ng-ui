import { NgModule, inject } from '@angular/core';
import { ActivatedRouteSnapshot, RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { StatusResolver } from './_resolver/status.resolver';
import { TaskResolver } from './_resolver/task.resolver';
import { TimesheetResolver } from './_resolver/timesheet.resolver';
import { UserResolver } from './_resolver/user.resolver';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'timesheet',
    pathMatch: 'full',
  },
  {
    path: 'timesheet',
    component: AppComponent,
    resolve: {
      status: (route: ActivatedRouteSnapshot) => {
        return inject(StatusResolver).resolve(route);
      },
      task: (route: ActivatedRouteSnapshot) => {
        return inject(TaskResolver).resolve(route);
      },
      timesheet: (route: ActivatedRouteSnapshot) => {
        return inject(TimesheetResolver).resolve(route);
      },
      user: (route: ActivatedRouteSnapshot) => {
        return inject(UserResolver).resolve(route);
      },
    },
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
