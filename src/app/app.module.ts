import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SearchboxComponent } from './searchbox/searchbox.component';
import { ListingComponent } from './listing/listing.component';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { FlexLayoutModule } from '@angular/flex-layout';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { statusReducer } from './_store/status/status.reducer';
import { StatusEffects } from './_store/status/status.effects';
import { HttpClientModule } from '@angular/common/http';
import { environment } from 'src/environment/environment';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { userReducer } from './_store/user/user.reducer';
import { taskReducer } from './_store/task/task.reducer';
import { timesheetReducer } from './_store/timesheet/timesheet.reducer';
import { TaskEffects } from './_store/task/task.effects';
import { UserEffects } from './_store/user/user.effects';
import { TimesheEtffects } from './_store/timesheet/timesheet.effects';
import { MatButtonModule } from '@angular/material/button';
import { CreateModalComponent } from './create-modal/create-modal.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { EditModalComponent } from './edit-modal/edit-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchboxComponent,
    ListingComponent,
    CreateModalComponent,
    EditModalComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    FlexLayoutModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    MatSelectModule,
    StoreModule.forRoot({
      status: statusReducer,
      user: userReducer,
      task: taskReducer,
      timesheet: timesheetReducer,
    }),
    EffectsModule.forRoot([
      StatusEffects,
      TaskEffects,
      TimesheEtffects,
      UserEffects,
    ]),
    !environment.production
      ? StoreDevtoolsModule.instrument({
          maxAge: 25,
        })
      : [],
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
