import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { createTimesheetInit } from '../_store/timesheet/timesheet.action';
import { ProjectState } from '../_store/state.index';
import { selectStatus } from '../_store/status/status.selector';
import { selectUser } from '../_store/user/user.selector';

import { Timesheet } from '../_models/timesheet.model';
import { Task } from '../_models/task.model';
import { Status } from '../_models/status.model';
import { User } from '../_models/user.model';

@Component({
  selector: 'app-create-modal',
  templateUrl: './create-modal.component.html',
  styleUrls: ['./create-modal.component.css'],
})
export class CreateModalComponent implements OnInit, OnDestroy {
  statuses$: Observable<Status[]>;
  users$: Observable<User[]>;

  constructor(
    public dialogRef: MatDialogRef<CreateModalComponent>,
    private store: Store<ProjectState>
  ) {}
  createTimesheetForm = new FormGroup(
    {
      projectName: new FormControl('', {
        validators: [Validators.required],
      }),
      taskName: new FormControl('', {
        validators: [Validators.required],
      }),
      dateFrom: new FormControl(null, {
        validators: [Validators.required],
      }),
      dateTo: new FormControl(null, {
        validators: [Validators.required],
      }),
      status: new FormControl('', {
        validators: [Validators.required],
      }),
      assignTo: new FormControl('', {
        validators: [Validators.required],
      }),
    },
    { validators: this.checktToAfterFrom() }
  );

  ngOnInit() {
    this.statuses$ = this.store.select(selectStatus);
    this.users$ = this.store.select(selectUser);
  }

  checktToAfterFrom(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const fromDate = control.get('dateFrom');
      const toDate = control.get('dateTo');
      if (fromDate.value > toDate.value) {
        control.get('dateTo').setErrors({ fromAfterto: true });
        return null;
      } else {
        // control.get('dateTo').setErrors(null);
        return null;
      }
    };
  }

  onSave() {
    let payloadTask: Task;
    payloadTask = {
      name: this.createTimesheetForm.value.taskName,
    };
    let payloadTimesheet: Timesheet;
    payloadTimesheet = {
      project: this.createTimesheetForm.value.projectName,
      fromDate: this.createTimesheetForm.value.dateFrom,
      toDate: this.createTimesheetForm.value.dateTo,
      statusId: this.createTimesheetForm.value.status,
      userId: this.createTimesheetForm.value.assignTo,
    };
    this.store.dispatch(
      createTimesheetInit({ task: payloadTask, timesheet: payloadTimesheet })
    );
  }
  ngOnDestroy() {}
}
